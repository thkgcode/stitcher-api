Stitcher API Wrapper
===

This library provides a wrapper around Stitcher's API endpoints.


Examples
---

Examples are located in the <examples> directory.


License
---

This project is licensed under the terms of the [MIT license](LICENSE.md).