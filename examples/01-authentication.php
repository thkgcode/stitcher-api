<?php declare(strict_types=1);

/**
 * Authenticates user against Stitcher's API.
 */

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$password = new Stitcher\Password();

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

$result = $client->CheckAuthentication([
    'email' => $config['email'],
    'udid' => $config['device-id'],
    'epx' => $password->encrypt($config['device-id'], $config['password'])
]);

if (!$result->error) {
    echo "Successfuly logged in!";
} else {
    echo "An error occurred: {$result->error}";
}

echo "\n\n";

if (PHP_SAPI != 'cli') {
    echo "<br><pre>";
}

print_r($result);
