<?php declare(strict_types=1);

/**
 * Fetches a user's information from the Stitcher API.
 */

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

$result = $client->LoadUserFromID([
    'uid' => $config['user-id']
]);

if (!$result->error) {
    echo "Successfuly fetched user!";
} else {
    echo "An error occurred: {$result->error}";
}

echo "\n\n";

if (PHP_SAPI != 'cli') {
    echo "<br><pre>";
}

print_r($result);
