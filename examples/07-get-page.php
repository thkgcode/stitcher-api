<?php declare(strict_types=1);

use Adduc\Stitcher;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config.php';

$client = new Stitcher\Client([
    'stitcher' => [
        'key' => $config['encrypt-key']
    ]
]);

$result = $client->GetPage([
    'id' => $config['page-id'],
    'uid' => $config['user-id'],
]);

echo "\n\n";

if (PHP_SAPI != 'cli') {
    echo "<br><pre>";
}

print_r($result);
