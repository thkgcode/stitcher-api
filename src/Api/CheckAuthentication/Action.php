<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\CheckAuthentication;

use Adduc\Stitcher\Api;

class Action extends Api\Action
{
    /**
     * @param Parameters $parameters
     * @return Response
     */
    public function __invoke(Parameters $parameters = null)
    {
        if (is_null($parameters)) {
            $parameters = new Parameters();
        }

        // must be GET, cannot be POST :/
        $method = "GET";
        $uri = "/Service/CheckAuthentication.php";
        $options = ['query' => $parameters->toArray()];

        if (!empty($parameters->guzzle_options)) {
            $options += $parameters->guzzle_options;
            unset($options['guzzle_options']);
        }

        $response_raw = $this->client->request($method, $uri, $options);
        $response_xml = simplexml_load_string((string)$response_raw->getBody());
        $response_user = new Response($response_xml);

        return $response_user;
    }
}
