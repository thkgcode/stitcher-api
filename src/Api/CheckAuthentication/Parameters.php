<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\CheckAuthentication;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property int
     */
    public $markStartup;

    /**
     * @property string
     */
    public $email;

    /**
     * @property string
     */
    public $epx;
}
