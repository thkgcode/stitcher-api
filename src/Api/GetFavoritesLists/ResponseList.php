<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFavoritesLists;

use Adduc\Stitcher\Api;

class ResponseList extends Api\Response
{
    /** @property int */
    public $id;
    /** @property string */
    public $name;
    /** @property int */
    public $editable;
    /** @property int */
    public $no_delete;
    /** @property int */
    public $unheard_favorites;
    /** @property string */
    public $smallThumbnailURL;
    /** @property string */
    public $thumbnailURL;
    /** @property int */
    public $Station_id;
    /** @property ? */
    public $subtitle;
    /** @property ResponseListThumbnail[] */
    public $thumbnails = [];
}
