<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFavoritesLists;

use Adduc\Stitcher\Api;

class ResponseListThumbnail extends Api\Response
{
    /**
     * @property int
     */
    public $id;

    /**
     * @property string
     */
    public $url;
}
