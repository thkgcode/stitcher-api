<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /**
     * @property int
     */
    public $app_version;

    /**
     * @property int
     */
    public $fid;

    /**
     * @property string
     */
    public $sess;

    /**
     * Episode to start from when listing, relative to most recent episode
     *
     * @property int
     */
    public $s;

    /**
     * Maximum number of episodes to return
     *
     * @property int
     */
    public $max_epi;

    /**
     * Season to fetch
     *
     * If empty, Stitcher returns 20 episodes from the first season
     * (not necessarily in chronological order).
     *
     * @property int
     */
    public $id_Season;
}
