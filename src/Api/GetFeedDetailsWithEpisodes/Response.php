<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    public $show_description;
    public $feed_details;
    public $feed;
    public $episodes;
    public $meta;
    public $maintenance_error;
    public $season;
}
