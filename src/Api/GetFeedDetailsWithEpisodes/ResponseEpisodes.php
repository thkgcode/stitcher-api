<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseEpisodes extends Api\Response
{
    public $episodes = [];

    /**
     * @property ResponseEpisodesMarker[]
     */
    public $markers = [];
}
