<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseFeed extends Api\Response
{
    public $id;
    public $name;
    public $feedDescription;
    public $title;
    public $episodeImage;
    public $episodeDescription;
    public $dateString;
    public $genre_id;
    public $genre_name;
    public $genre_color;
    public $thumbnailURL;
    public $smallThumbnailURL;
    public $largeThumbnailURL;
    public $seokey;
    public $id_Episode;
    public $description;
    public $cached;
    public $episodeURL;
    public $episodeURL_original;
    public $headerSizeInBytes;
    public $duration;
    public $commentCount;
    public $explicit;
    public $skippable;
    public $published;
    public $upRating;
    public $displayAds;
    public $adsAllowed;
    public $id_RSSProvider;
    public $color;
    public $banner;
    public $authMethod;
    public $authRequired;
    public $feedImage;
    public $id_RSSFeed_premium;
    public $episodeSeokey;
    public $isFavorite;
    public $fave_lid;
    public $freemium;
    public $episodeCount;
    public $imageURL;
    public $premium;
    public $donationURL;
    public $totalDuration;
    public $donationEmail;
    public $autoSeason;
    public $sortDirection;

    /**
     * @property ResponseFeedEpisode[]
     */
    public $episodes = [];

    /**
     * @property ResponseFeedSeason[]
     */
    public $seasons = [];

    /**
     * @property ResponseFeedMarker[]
     */
    public $marker;
}
