<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseFeedEpisode extends Api\Response
{
    public $id;
    public $duration;
    public $episodeImage;
    public $published;
    public $dateString;
    public $headerSizeInBytes;
    public $bitrate;
    public $url;
    public $episodeURL_original;
    public $title;
    public $description;
    public $id_Season;
    public $episodeNumber;
    public $banner;
    public $unlisted;
    public $originalDescription;
    public $expiration;
    public $noCache;
}
