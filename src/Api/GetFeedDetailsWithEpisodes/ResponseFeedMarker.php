<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetFeedDetailsWithEpisodes;

use Adduc\Stitcher\Api;

class ResponseFeedMarker extends Api\Response
{
    public $id;
    public $heard;
    public $offset;
    public $autoGenOffset;
}
