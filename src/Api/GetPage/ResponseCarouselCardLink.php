<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseCarouselCardLink extends Api\Response
{
    public $url;
}
