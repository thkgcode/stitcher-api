<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetPage;

use Adduc\Stitcher\Api;

class ResponseStationFeed extends Api\Response
{
    public $id;
    public $dateString;
    public $thumbnailURL;
    public $smallThumbnailURL;
    public $largeThumbnailURL;
    public $seokey;
    public $banner;
    public $authMethod;
    public $authRequired;
    public $skippable;
    public $published;
    public $upRating;
    public $displayAds;
    public $adsAllowed;
    public $id_RSSProvider;
    public $color;
    public $name;
    public $description;
    public $explicit;
    public $id_RSSFeed_premium;
    public $feedDescription;
    public $title;
    public $episodeImage;
    public $episodeDescription;
    public $id_Episode;
    public $cached;
    public $episodeURL;
    public $episodeURL_original;
    public $bitrate;
    public $headerSizeInBytes;
    public $duration;
    public $commentCount;
    public $totalDuration;
    public $donationURL;
    public $noReport;
    public $unlisted;
    public $freemium;
    public $imageURL;
    public $premium;
    public $episodeCount;
    public $autoSeason;

    public $episodes = [];
    public $seasons = [];
}
