<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    /**
     * @property string
     */
    public $name;

    /**
     * @proeprty int
     */
    public $id;

    /**
     * @property int
     */
    public $androidPlaylistRefreshRate;

    /**
     * @property int
     */
    public $editable;

    /**
     * @property int
     */
    public $no_delete;

    /**
     * @property ResponseUser
     */
    public $user;

    /**
     * @property ResponseStation
     */
    public $station;

    /**
     * @property ResponseFeed[]
     */
    public $feeds = [];

    /**
     * @property int
     */
    public $code;

    /**
     * @property string
     */
    public $text;
}
