<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class ResponseStation extends Api\Response
{
    /**
     * @property int
     */
    public $id;

    /**
     * @property string
     */
    public $name;

    /**
     * @property int
     */
    public $count;

    /**
     * @property int
     */
    public $sponsored;
}
