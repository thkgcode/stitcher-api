<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetStationPlayList;

use Adduc\Stitcher\Api;

class ResponseUser extends Api\Response
{
    /**
     * @property int
     */
    public $id;
}
