<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\GetSubscriptionStatus;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    // Same parameters as Api\Parameters
}
