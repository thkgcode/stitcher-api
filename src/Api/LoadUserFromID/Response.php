<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\LoadUserFromID;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    const ERROR_USER_NOT_FOUND = 'userNotFound';

    /**
     * Indicates type of error, if any.
     *
     * @property string
     */
    public $error;

    /**
     * User ID
     *
     * @property int
     */
    public $id;

    /**
     * @property string
     */
    public $email;

    /**
     * Phone number
     *
     * May not be collected anymore?
     *
     * @property string
     */
    public $phone;

    /**
     * @property int
     */
    public $optin;

    /**
     * @property int
     */
    public $explicit;

    /**
     * @property int
     */
    public $favorites_list_count;

    /**
     * @property int
     */
    public $unheard_favorites;

    /**
     * @property int
     */
    public $shares;

    /**
     * @property int
     */
    public $episodeCount;

    /**
     * @property int
     */
    public $totalListeningSeconds;

    /**
     * @property int
     */
    public $facebookShareEnabled;

    /**
     * @property int
     */
    public $shareListenFB;

    /**
     * @property int
     */
    public $shareThumbsFB;

    /**
     * @property int
     */
    public $shareFavsFB;

    /**
     * @property int
     */
    public $numNewFriends;

    /**
     * @property int
     */
    public $forceFavoritesWizard;

    /**
     * @property int
     */
    public $defaultLaunchPage;

    /**
     * @property int
     */
    public $subscriptionState;

    /**
     * @property int
     */
    public $inviteFlowID;

    /**
     * @property string
     */
    public $subscriptionExpiration;

    /**
     * @property string
     */
    public $subscriptionPlatform;

    /**
     * @property string
     */
    public $subscriptionSource;

    /**
     * @property int
     */
    public $population;

    /**
     * @property int
     */
    public $testGroup;

    /**
     * @property int
     */
    public $howl_favorites_id;
}
