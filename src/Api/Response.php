<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api;

use Doctrine\Common\Inflector\Inflector;
use SimpleXMLElement;

abstract class Response
{
    /**
     * @param mixed $data
     */
    public function __construct($data = [])
    {
        if ($data instanceof SimpleXMLElement) {
            foreach ($data->attributes() as $key => $attribute) {
                $this->$key = strval($attribute);
            }

            foreach ($data as $key => $value) {
                if (!empty($this->$key)) {
                    trigger_error(sprintf(
                        "%s::%s is already set. May need to be an array. [%s]",
                        get_called_class(),
                        $key,
                        json_encode($data)
                    ));
                }

                if (trim(strval($value))) {
                    $this->$key = strval($value);
                    continue;
                }

                $class = get_called_class() . Inflector::classify($key);
                if (class_exists($class)) {
                    $this->$key = new $class($value);
                    continue;
                }

                if ($value->children() || $value->attributes()) {
                    trigger_error("Missing class: ${class}");
                }
            }
        } elseif (is_array($data)) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value)
    {
        if (strpos($key, '.') !== false) {
            $key = str_replace('.', '_', $key);
            $this->$key = $value;
            return;
        }

        $plural = Inflector::pluralize($key);

        if (isset($this->$plural) && is_array($this->$plural)) {
            $this->{$plural}[] = $value;
            return;
        }

        $msg = "Missing attribute: " . get_called_class() . "::\${$key}";
        $msg .= "[" . json_encode($this) . "]";

        trigger_error($msg);
    }

    /**
     * Loops through response properties recursively to find instances
     * of a class.
     *
     * @param string $class
     * @return array
     */
    public function extractInstanceOf($class)
    {
        $extract = function ($data, array &$carry = []) use (&$extract, $class) {

            if ($data instanceof $class) {
                $carry[] = $data;
                return $carry;
            }

            if (is_object($data)) {
                $data = get_object_vars($data);
            }

            foreach ($data as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $extract($value, $carry);
                }
            }

            return $carry;
        };

        return $extract(get_object_vars($this));
    }
}
