<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class Parameters extends Api\Parameters
{
    /** @property string */
    public $term;

    /** @property int */
    public $s;

    /** @property int */
    public $c;
}
