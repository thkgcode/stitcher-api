<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class ResponseFeedEpisode extends Api\Response
{
    public $id;
    public $duration;
    public $episodeImage;
    public $published;
    public $dateString;
    public $headerSizeInBytes;
    public $bitrate;
    public $url;
    public $episodeURL_original;
    public $title;
    public $description;
    public $unlisted;
    public $id_Season;
    public $episodeNumber;
    public $originalDescription;
    public $banner;
    public $expiration;
    public $noCache;
}
