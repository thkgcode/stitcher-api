<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\Search;

use Adduc\Stitcher\Api;

class ResponseFeedMarker extends Api\Response
{
    public $id;
    public $offset;
    public $autoGenOffset;
    public $heard;
}
