<?php declare(strict_types=1);

namespace Adduc\Stitcher\Api\StartupChecks;

use Adduc\Stitcher\Api;

class Response extends Api\Response
{
    public $startupOK;
    public $newRelic;
    public $heard_percent;
    public $heard_time_cutoff;
    public $stations_updated;
    public $content_stations_updated;
    public $comScore;
    public $credits;
    public $creditsTitleText;
    public $force_registration;
    public $days_between_prompts;
    public $actions_between_prompts;
    public $preloader_completionReport;
    public $request_register;
    public $tutorial;
    public $favorites_wizard;
    public $match_podcasts;
    public $min_matching_podcasts;
    public $sales_carousel;
    public $premium_page;
    public $premium_page_banner;
    public $adRefreshInterval;
    public $currentAppStoreProductID;
    public $currentAppStoreDurationString;
    public $min_adcart_timer;
    public $enable_samsung_enhanced_player;
    public $version_min;
    public $videoAds;
    public $adPauseThreshold;
    public $adSkipTimer;
    public $adMediaTimeout;
    public $minAdInterval;
    public $simple_player_override;
    public $live_player_no_append;
    public $standard_player_no_network_seek;
    public $intraFeedSearch;
}
