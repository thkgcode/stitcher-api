<?php declare(strict_types=1);

namespace Adduc\Stitcher\Middleware;

use Psr\Http\Message\RequestInterface;
use function GuzzleHttp\Psr7\parse_query;
use function GuzzleHttp\Psr7\build_query;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\Base;
use function GuzzleHttp\Psr7\stream_for;

class Encrypt
{
    /** @var string */
    protected $string;

    /** @var callable */
    protected $handler;

    /** @var Base */
    protected $crypt;

    public function __construct(array $config)
    {
        if (strlen($config['stitcher']['key'] ?? '') !== 16) {
            throw new \InvalidArgumentException("stitcher.key must be a 16 char length string.");
        }

        $this->key = $config['stitcher']['key'];
    }

    public function __invoke(callable $handler)
    {
        $this->handler = $handler;
        return [$this, 'request'];
    }

    public function request(RequestInterface $request, array $options)
    {
        $handler = $this->handler;

        if (strtolower($request->getMethod()) == 'get') {
            $to_encrypt = parse_query($request->getUri()->getQuery());
        } else {
            $to_encrypt = parse_query($request->getBody());
        }

        $crypt = $this->getCrypt();

        $to_return = [
            'version' => $to_encrypt['version']
        ];

        unset($to_encrypt['version']);

        $to_encrypt = json_encode($to_encrypt);
        $to_return['params'] = base64_encode($crypt->encrypt($to_encrypt));

        if (strtolower($request->getMethod()) == 'get') {
            $uri = $request->getUri()->withQuery(build_query($to_return));
            $request = $request->withUri($uri);
        } else {
            $request = $request->withBody(stream_for(build_query($to_return)));
        }

        return $handler($request, $options);
    }

    protected function getCrypt() : Base
    {
        if (is_null($this->crypt)) {
            $this->crypt = new AES();
            $this->crypt->setKey($this->key);
            $this->crypt->setIV($this->key);
        }

        return $this->crypt;
    }
}
